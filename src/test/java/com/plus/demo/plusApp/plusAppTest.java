package com.plus.demo.plusApp;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class plusAppTest {
	/** * Initialization */
	@Before
	public void setUp() {
		new App();
	}

	/** * Test case for add method */
	@Test
	public void test() {
		int i = App.add(2, 8);
		assertEquals(10, i);
	}

	/** * destroy the object */
	@After
	public void tearDown() {
	}

}
